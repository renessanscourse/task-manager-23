package ru.ovechkin.tm.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.entitiesForTest.UsersForTest;

public class UserRepositoryForTest {

    private final IUserRepository userRepository = new UserRepository();

    private final UsersForTest usersForTest = new UsersForTest();

    private final User testUser = usersForTest.getUserUser();

    @Before
    public void prepareRepository() {
        userRepository.clear();
        userRepository.add(testUser);
    }

    @Test
    public void testAdd() {
        Assert.assertTrue(userRepository.findAll().contains(testUser));
    }

    @Test
    public void testFindById() {
        Assert.assertEquals(testUser, userRepository.findById(testUser.getId()));
    }

    @Test
    public void testFindByLogin() {
        Assert.assertEquals(testUser, userRepository.findByLogin(testUser.getLogin()));
    }

    @Test
    public void testRemoveUser() {
        userRepository.removeUser(testUser);
        Assert.assertFalse(userRepository.findAll().contains(testUser));
    }

    @Test
    public void testRemoveById() {
        userRepository.removeById(testUser.getId());
        Assert.assertFalse(userRepository.findAll().contains(testUser));
    }

    @Test
    public void testRemoveByLogin() {
        userRepository.removeByLogin(testUser.getLogin());
        Assert.assertFalse(userRepository.findAll().contains(testUser));
    }

}