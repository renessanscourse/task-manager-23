package ru.ovechkin.tm.repository;

import org.junit.*;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.constant.TestConst;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entitiesForTest.ProjectsForTest;

import java.util.List;

public class ProjectRepositoryTest {

    private final IProjectRepository projectRepository = new ProjectRepository();

    private  final ProjectsForTest projectsForTest = new ProjectsForTest();

    private  final Project project1 = projectsForTest.getProject1();

    private  final Project project2 = projectsForTest.getProject2();

    private final Project project3 = projectsForTest.getProject3();

    public List<Project> listOfTestProjects() {
        return projectsForTest.getListOfTestProjects();
    }

    @Before
    public void clearRepository() {
        projectRepository.clear();
        projectRepository.merge(listOfTestProjects());
    }

    @Test
    public void testAdd() {
        projectRepository.clear();
        projectRepository.add(project1.getUserId(), project1);
        Assert.assertEquals(project1, projectRepository.findById(project1.getUserId(), project1.getId()));
    }

    @Test
    public void testFindUserProjects() {
        final List<Project> projectsOfUsers = listOfTestProjects();
        projectsOfUsers.remove(project3);// убираем проект с ненужным userId
        Assert.assertEquals(projectsOfUsers, projectRepository.findUserProjects(TestConst.USER_ID_USER));
    }

    @Test
    public void testRemoveAll() {
        projectRepository.merge(listOfTestProjects());
        final List<Project> projects = listOfTestProjects();
        projects.removeAll(listOfTestProjects());
        projectRepository.removeAll(TestConst.USER_ID_USER);
        Assert.assertEquals(projects, projectRepository.findUserProjects(TestConst.USER_ID_USER));
    }

    @Test
    public void testFindById() {
        projectRepository.clear();
        projectRepository.merge(project1);
        Assert.assertEquals(project1, projectRepository.findById(project1.getUserId(), project1.getId()));
        Assert.assertNull(projectRepository.findById(project2.getUserId(), project2.getId()));
        Assert.assertNull(projectRepository.findById(project2.getUserId(), project3.getId()));
    }

    @Test
    public void testFindByIndex() {
        Assert.assertEquals(project3, projectRepository.findByIndex(project3.getUserId(), 1));
        Assert.assertNull(projectRepository.findByIndex(project3.getUserId(), 2));
    }

    @Test
    public void testFindByName() {
        Assert.assertEquals(project3, projectRepository.findByName(project3.getUserId(), project3.getName()));
        Assert.assertNull(projectRepository.findByName(project3.getUserId(), "project3.getName())"));
        Assert.assertNull(projectRepository.findByName(project2.getUserId(), project3.getName()));
    }

    @Test
    public void testRemoveById() {
        projectRepository.removeById(project1.getUserId(), project1.getId());
        Assert.assertNull(projectRepository.findById(project1.getUserId(), project1.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        projectRepository.removeByIndex(project1.getUserId(), 1);
        Assert.assertNull(projectRepository.findById(project1.getUserId(), project1.getId()));
    }

    @Test
    public void testRemoveByName() {
        projectRepository.removeByName(project1.getUserId(), project1.getName());
        Assert.assertNull(projectRepository.findByName(project1.getUserId(), project1.getName()));
    }

}