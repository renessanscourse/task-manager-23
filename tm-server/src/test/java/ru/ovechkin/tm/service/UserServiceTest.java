package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.user.*;

import java.util.List;

public class UserServiceTest extends AbstractEntitiesForTest {

    private List<User> listOfUsers() {
        return usersForTest.getListOfUsers();
    }

    @Before
    public void prepareService() {
        userService.clear();
    }

    @Test
    public void testFindByIdPositive() {
        userService.mergeCollection(listOfUsers());
        Assert.assertEquals(user, userService.findById(user.getId()));
    }

    @Test
    public void testFindByIdNegative() {
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.findById(null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.findById("")
        );
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Test
    public void testFindByLoginPositive() {
        userService.mergeCollection(listOfUsers());
        Assert.assertEquals(user, userService.findByLogin(user.getLogin()));
    }

    @Test
    public void testFindByLoginNegative() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.findByLogin(null)
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.findByLogin("")
        );
        Assert.assertNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void testRemoveUserPositive() {
        userService.mergeCollection(listOfUsers());
        userService.removeUser(user);
        Assert.assertFalse(userService.findAll().contains(user));
    }

    @Test
    public void testRemoveUserNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> userService.removeUser(null)
        );
    }

    @Test
    public void testRemoveByIdPositive() {
        userService.mergeCollection(listOfUsers());
        userService.removeById(admin.getId(), user.getId());
        Assert.assertFalse(userService.findAll().contains(user));
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.removeById(null, user.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.removeById("", user.getId())
        );
        userService.mergeOne(admin);
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.removeById(admin.getId(), null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.removeById(admin.getId(), "")
        );
        Assert.assertThrows(
                UserDoesNotExistException.class,
                () -> userService.removeById(admin.getId(), user.getId())
        );
        Assert.assertThrows(
                SelfRemovingException.class,
                () -> userService.removeById(admin.getId(), admin.getId()));
    }

    @Test
    public void testRemoveByLoginPositive() {
        userService.mergeCollection(listOfUsers());
        userService.removeByLogin(admin.getId(), user.getLogin());
        Assert.assertFalse(userService.findAll().contains(user));
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.removeByLogin(admin.getId(), null)
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.removeByLogin(admin.getId(), "")
        );
        Assert.assertThrows(
                UserDoesNotExistException.class,
                () -> userService.removeByLogin(admin.getId(), user.getLogin())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.removeByLogin("", user.getLogin())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.removeByLogin(null, user.getLogin())
        );
        userService.mergeCollection(listOfUsers());
        userService.create("admin1", "admin1", Role.ADMIN);
        Assert.assertThrows(
                AdminRemovingException.class,
                () -> userService.removeByLogin(admin.getId(), "admin1")
        );
        Assert.assertThrows(
                SelfRemovingException.class,
                () -> userService.removeByLogin(admin.getId(), admin.getLogin())
        );
        userService.create("user", "user");
        Assert.assertThrows(
                AccessDeniedException.class,
                () -> userService.lockUserByLogin(user.getId(), "user")
        );
    }

    @Test
    public void testCreateUserPositive() {
        userService.create(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void testCreateUserNegative() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", "password")
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, "password")
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(user.getLogin(), "")
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(user.getLogin(), null)
        );
    }

    @Test
    public void testCreateWithEmailPositive() {
        userService.create(user.getLogin(), user.getPasswordHash(), user.getEmail());
        Assert.assertNotNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void testCreateWithEmailNegative() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", "password", "email")
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, "password", "email")
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(user.getLogin(), "", "email")
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(user.getLogin(), null, "email")
        );
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> userService.create(user.getLogin(), "password", "")
        );
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> userService.create(user.getLogin(), "password", (String) null)
        );
    }

    @Test
    public void testCreateWithRolePositive() {
        userService.create(user.getLogin(), user.getPasswordHash(), Role.ADMIN);
        Assert.assertNotNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void testCreateWithRoleNegative() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", "password", Role.USER)
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, "password", Role.USER)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(user.getLogin(), "", Role.USER)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(user.getLogin(), null, Role.USER)
        );
        Assert.assertThrows(
                RoleEmptyException.class,
                () -> userService.create(user.getLogin(), "password", (Role) null)
        );
    }

    @Test
    public void testLockUserByLoginPositive() {
        userService.mergeCollection(listOfUsers());
        userService.lockUserByLogin(admin.getId(), user.getLogin());
        Assert.assertTrue(userService.findByLogin(user.getLogin()).getLocked());
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.lockUserByLogin(admin.getId(), null)
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.lockUserByLogin(admin.getId(), "")
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.lockUserByLogin("", user.getLogin())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.lockUserByLogin(null, user.getLogin())
        );
        Assert.assertThrows(
                UserDoesNotExistException.class,
                () -> userService.lockUserByLogin(admin.getId(), user.getLogin())
        );
        userService.mergeCollection(listOfUsers());
        userService.create("admin1", "admin1", Role.ADMIN);
        Assert.assertThrows(
                AdminRemovingException.class,
                () -> userService.removeByLogin(admin.getId(), "admin1")
        );
        Assert.assertThrows(
                SelfBlockingException.class,
                () -> userService.lockUserByLogin(admin.getId(), admin.getLogin())
        );
        userService.create("user", "user");
        Assert.assertThrows(
                AccessDeniedException.class,
                () -> userService.lockUserByLogin(user.getId(), "user")
        );
    }

    @Test
    public void testUnLockUserByLoginPositive() {
        userService.mergeCollection(listOfUsers());
        userService.lockUserByLogin(admin.getId(), user.getLogin());
        userService.unLockUserByLogin(admin.getId(), user.getLogin());
        Assert.assertFalse(userService.findByLogin(user.getLogin()).getLocked());
    }

    @Test
    public void testUnLockUserByLoginNegative() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.unLockUserByLogin(admin.getId(), null)
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.unLockUserByLogin(admin.getId(), "")
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.unLockUserByLogin("", user.getLogin())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.unLockUserByLogin(null, user.getLogin())
        );
        Assert.assertThrows(
                UserDoesNotExistException.class,
                () -> userService.unLockUserByLogin(admin.getId(), user.getLogin())
        );
        userService.mergeCollection(listOfUsers());
        Assert.assertThrows(
                AccessDeniedException.class,
                () -> userService.unLockUserByLogin(user.getId(), user.getLogin())
        );
    }

}