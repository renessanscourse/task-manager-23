package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.service.IPropertyService;

public class PropertyServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    @Before
    public void initStream() throws Exception {
        propertyService.init();
    }

    @Test
    public void testGetServicePortPositive() {
        Assert.assertEquals((Integer) 8080, propertyService.getServicePort());
    }

    @Test
    public void testGetServiceHostPositive() {
        Assert.assertEquals("localhost", propertyService.getServiceHost());
    }

    @Test
    public void testGetSessionSaltPositive() {
        Assert.assertEquals("agjdasf", propertyService.getSessionSalt());
    }

    @Test
    public void testGetSessionCyclePositive() {
        Assert.assertEquals((Integer) 13, propertyService.getSessionCycle());
    }

}