package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.constant.TestConst;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.exeption.empty.*;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceTest extends AbstractEntitiesForTest {

    private List<Task> listOfTestTasks() {
        return tasksForTest.getListOfTestTasks();
    }

    @Before
    public void prepareService() {
        taskService.clear();
    }

    @Test
    public void testCreateWithNamePositive() {
        taskService.create(user.getId(), "taskName1");
        final Task createdTask = taskService.findTaskByName(user.getId(), "taskName1");
        Assert.assertNotNull(createdTask);
        Assert.assertEquals("taskName1", createdTask.getName());
    }

    @Test
    public void testCreateWithNameNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.create(null, TestConst.TEST_STRING));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.create("", TestConst.TEST_STRING));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.create(TestConst.TEST_STRING, null));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.create(TestConst.TEST_STRING, ""));
    }

    @Test
    public void testCreateWithNameAndDescriptionPositive() {
        taskService.create(user.getId(), "taskName1", "descriptionForTask1");
        final Task createdTask = taskService.findTaskByIndex(user.getId(), 1);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals("descriptionForTask1", createdTask.getDescription());
    }

    @Test
    public void testCreateWithNameAndDescriptionNegative() {
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.create(TestConst.TEST_STRING, TestConst.TEST_STRING, null));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.create(TestConst.TEST_STRING, TestConst.TEST_STRING, ""));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.create(null, TestConst.TEST_STRING));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.create("", TestConst.TEST_STRING));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.create(TestConst.TEST_STRING, null));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.create(TestConst.TEST_STRING, ""));
    }

    @Test
    public void testFindUserTasksPositive() {
        final ITaskService mock = Mockito.mock(TaskService.class);
        Mockito
                .when(mock.findUserTasks(task1.getUserId()))
                .thenReturn(listOfTestTasks());
        Assert.assertEquals(
                listOfTestTasks(),
                mock.findUserTasks(task1.getUserId()));
    }

    @Test
    public void testFindUserTasksNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.findUserTasks(null));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.findUserTasks(""));
    }

    @Test
    public void testRemoveAllTasksPositive() {
        taskService.mergeCollection(listOfTestTasks());
        taskService.removeAllTasks(task1.getUserId());
        Assert.assertEquals(
                new ArrayList<>(),
                taskService.findUserTasks(task1.getUserId()));
    }

    @Test
    public void testRemoveAllTasksNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.removeAllTasks(null));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.removeAllTasks(""));
    }

    @Test
    public void testFindTaskByIdPositive() {
        taskService.mergeCollection(listOfTestTasks());
        Assert.assertEquals(
                task1,
                taskService.findTaskById(task1.getUserId(), task1.getId()));
    }

    @Test
    public void testFindTaskByIdNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.findTaskById(null, TestConst.TEST_STRING));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.findTaskById("", TestConst.TEST_STRING));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> taskService.findTaskById(task1.getUserId(), null));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> taskService.findTaskById(task1.getUserId(), ""));
    }

    @Test
    public void testFindTaskByIndexPositive() {
        taskService.mergeCollection(listOfTestTasks());
        Assert.assertEquals(
                task1,
                taskService.findTaskByIndex(task1.getUserId(), 1));
    }

    @Test
    public void testFindTaskByIndexNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.findTaskByIndex(null, 2));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.findTaskByIndex("", 2));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.findTaskByIndex(task1.getUserId(), null));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.findTaskByIndex(task1.getUserId(), 0));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.findTaskByIndex(task1.getUserId(), -22));
    }

    @Test
    public void findTaskByNamePositive() {
        taskService.mergeCollection(listOfTestTasks());
        Assert.assertEquals(
                task1,
                taskService.findTaskByName(task1.getUserId(), task1.getName()));
    }

    @Test
    public void findTaskByNameNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.findTaskByName(null, task1.getName()));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.findTaskByName("", task1.getName()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.findTaskByName(task1.getUserId(), null));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.findTaskByName(task1.getUserId(), ""));
    }

    @Test
    public void testUpdateTaskByIdPositive() {
        taskService.mergeOne(task1);
        final Task updatedTask1 = taskService.updateTaskById(
                task1.getUserId(), task1.getId(), "newName", "newDescription"
        );
        taskService.updateTaskById(
                task1.getUserId(), task1.getId(), "newName", "newDescription"
        );
        Assert.assertEquals(updatedTask1, task1);
        Assert.assertEquals(updatedTask1.getDescription(), task1.getDescription());
    }

    @Test
    public void testUpdateTaskByIdNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.updateTaskById(
                        null,
                        task1.getId(),
                        task1.getName(),
                        task1.getDescription()));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.updateTaskById(
                        "",
                        task1.getId(),
                        task1.getName(),
                        task1.getDescription()));
        taskService.mergeOne(task1);
        Assert.assertThrows(
                IdEmptyException.class,
                () -> taskService.updateTaskById(
                        task1.getUserId(),
                        null,
                        task1.getName(),
                        task1.getDescription()));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> taskService.updateTaskById(
                        task1.getUserId(),
                        "",
                        task1.getName(),
                        task1.getDescription()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.updateTaskById(
                        task1.getUserId(),
                        task1.getId(),
                        "",
                        task1.getDescription()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.updateTaskById(
                        task1.getUserId(),
                        task1.getId(),
                        null,
                        task1.getDescription()));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.updateTaskById(
                        task1.getUserId(),
                        task1.getId(),
                        "newName",
                        null));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.updateTaskById(
                        task1.getUserId(),
                        task1.getId(),
                        "newName",
                        ""));
    }

    @Test
    public void testUpdateTaskByIndexPositive() {
        taskService.mergeOne(task1);
        final Task updatedTask1 = taskService.updateTaskByIndex(
                task1.getUserId(), 1, "newName", "newDescription"
        );
        taskService.updateTaskByIndex(
                task1.getUserId(), 1, "newName", "newDescription"
        );
        Assert.assertEquals(updatedTask1, task1);
        Assert.assertEquals(updatedTask1.getDescription(), task1.getDescription());
    }

    @Test
    public void testUpdateTaskByIndexNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        null,
                        1,
                        task1.getName(),
                        task1.getDescription()));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        "",
                        1,
                        task1.getName(),
                        task1.getDescription()));
        taskService.mergeOne(task1);
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        task1.getUserId(),
                        null,
                        task1.getName(),
                        task1.getDescription()));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        task1.getUserId(),
                        0,
                        task1.getName(),
                        task1.getDescription()));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        task1.getUserId(),
                        -1,
                        task1.getName(),
                        task1.getDescription()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        task1.getUserId(),
                        1,
                        "",
                        task1.getDescription()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        task1.getUserId(),
                        1,
                        null,
                        task1.getDescription()));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        task1.getUserId(),
                        1,
                        "newName",
                        null));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.updateTaskByIndex(
                        task1.getUserId(),
                        1,
                        "newName",
                        ""));
    }

    @Test
    public void testRemoveByIdPositive() {
        taskService.mergeCollection(listOfTestTasks());
        taskService.removeTaskById(task1.getUserId(), task1.getId());
        Assert.assertNull(taskService.findTaskById(task1.getUserId(), task1.getId()));
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.removeTaskById(null, TestConst.TEST_STRING));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.removeTaskById("", TestConst.TEST_STRING));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> taskService.removeTaskById(task1.getUserId(), null));
        Assert.assertThrows(
                IdEmptyException.class,
                () -> taskService.removeTaskById(task1.getUserId(), ""));
    }

    @Test
    public void testRemoveTaskByIndexPositive() {
        taskService.mergeCollection(listOfTestTasks());
        taskService.removeTaskByIndex(task1.getUserId(), 1);
        Assert.assertNull(taskService.findTaskById(task1.getUserId(), task1.getId()));
    }

    @Test
    public void testRemoveTaskByIndexNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.removeTaskByIndex(null, 2));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.removeTaskByIndex("", 2));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.removeTaskByIndex(task1.getUserId(), null));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.removeTaskByIndex(task1.getUserId(), 0));
        Assert.assertThrows(
                IndexEmptyException.class,
                () -> taskService.removeTaskByIndex(task1.getUserId(), -22));
    }

    @Test
    public void testRemoveTaskByNamePositive() {
        taskService.mergeCollection(listOfTestTasks());
        taskService.removeTaskByName(task1.getUserId(), task1.getName());
        Assert.assertNull(taskService.findTaskById(task1.getUserId(), task1.getId()));
    }

    @Test
    public void testRemoveTaskByNameNegative() {
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.removeTaskByName(null, task1.getName()));
        Assert.assertThrows(
                UserEmptyException.class,
                () -> taskService.removeTaskByName("", task1.getName()));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.removeTaskByName(task1.getUserId(), null));
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.removeTaskByName(task1.getUserId(), ""));
    }

}