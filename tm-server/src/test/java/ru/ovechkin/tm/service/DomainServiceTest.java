package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.service.IDomainService;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.dto.Domain;

public class DomainServiceTest extends AbstractEntitiesForTest {

    @Before
    public void prepareServices() {
        projectService.clear();
        taskService.clear();
        userService.clear();
        projectService.mergeCollection(testProjects);
        taskService.mergeCollection(testTasks);
        userService.mergeCollection(testUsers);
    }

    @Test
    public void testSave() {
        final Domain domain = new Domain();
        domainService.save(domain);
        Assert.assertEquals(testProjects.toString(), domain.getProjects().toString());
        Assert.assertEquals(testTasks.toString(), domain.getTasks().toString());
        Assert.assertEquals(testUsers.toString(), domain.getUsers().toString());
    }

    @Test
    public void testLoad() {
        final Domain domain = new Domain();
        domainService.save(domain);
        final IProjectService newProjectService = serviceLocator.getProjectService();
        newProjectService.clear();
        final ITaskService newTaskService = serviceLocator.getTaskService();
        newTaskService.clear();
        final IUserService newUserService = serviceLocator.getUserService();
        newUserService.clear();
        final IDomainService newDomainService = new DomainService(newTaskService, newProjectService, newUserService);
        newDomainService.load(domain);
        Assert.assertEquals(testProjects.toString(), newProjectService.findAll().toString());
        Assert.assertEquals(testTasks.toString(), newTaskService.findAll().toString());
        Assert.assertEquals(testUsers.toString(), newUserService.findAll().toString());
    }

}