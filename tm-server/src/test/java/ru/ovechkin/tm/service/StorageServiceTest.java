package ru.ovechkin.tm.service;

import org.junit.*;
import org.junit.runners.MethodSorters;
import ru.ovechkin.tm.constant.PathToSavedFile;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StorageServiceTest extends AbstractEntitiesForTest {

    @Before
    public void prepareUsers() {
        testUsers.get(0).setId("ConstantUserId");
        testUsers.get(1).setId("ConstantAdminId");
    }

    @AfterClass
    public static void removeSavedFiles() throws IOException {
        final File base64Save = new File(PathToSavedFile.DATA_BASE64);
        Files.deleteIfExists(base64Save.toPath());
        final File binSave = new File(PathToSavedFile.DATA_BIN);
        Files.deleteIfExists(binSave.toPath());
        final File jJSave = new File(PathToSavedFile.DATA_JSON_JAXB);
        Files.deleteIfExists(jJSave.toPath());
        final File jMSave = new File(PathToSavedFile.DATA_JSON_MAPPER);
        Files.deleteIfExists(jMSave.toPath());
        final File xJSave = new File(PathToSavedFile.DATA_XML_JAXB);
        Files.deleteIfExists(xJSave.toPath());
        final File xMSave = new File(PathToSavedFile.DATA_XML_MAPPER);
        Files.deleteIfExists(xMSave.toPath());
    }

    @Test
    public void testDataBase64ASave() throws Exception {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        storageService.dataBase64Save();
        final File file = new File(PathToSavedFile.DATA_BASE64);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataBase64BLoad() throws Exception {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        storageService.dataBase64Load();
        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataBinaryASave() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        storageService.dataBinarySave();
        final File file = new File(PathToSavedFile.DATA_BASE64);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataBinaryBLoad() throws IOException, ClassNotFoundException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        storageService.dataBinaryLoad();
        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataJsonJaxbASave() throws IOException, JAXBException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        storageService.dataJsonJaxbSave();
        final File file = new File(PathToSavedFile.DATA_BASE64);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataJsonJaxbBLoad() throws JAXBException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        storageService.dataJsonJaxbLoad();
        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataJsonMapperBLoad() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        storageService.dataJsonMapperLoad();
        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataJsonMapperASave() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        storageService.dataJsonMapperSave();
        final File file = new File(PathToSavedFile.DATA_BASE64);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataXmlJaxbASave() throws IOException, JAXBException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        storageService.dataXmlJaxbSave();
        final File file = new File(PathToSavedFile.DATA_BASE64);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataXmlJaxbBLoad() throws IOException, JAXBException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        storageService.dataXmlJaxbLoad();
        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

    @Test
    public void testDataXmlMapperASave() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().mergeCollection(testUsers);
        serviceLocator.getProjectService().mergeCollection(projectsForTest.getListOfTestProjects());
        serviceLocator.getTaskService().mergeCollection(tasksForTest.getListOfTestTasks());
        storageService.dataXmlMapperSave();
        final File file = new File(PathToSavedFile.DATA_BASE64);
        Assert.assertTrue(file.isFile());
    }

    @Test
    public void testDataXmlMapperBLoad() throws IOException {
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        storageService.dataXmlMapperLoad();
        Assert.assertEquals(testUsers.toString(), serviceLocator.getUserService().findAll().toString());
        Assert.assertEquals(testProjects.toString(), serviceLocator.getProjectService().findAll().toString());
        Assert.assertEquals(testTasks.toString(), serviceLocator.getTaskService().findAll().toString());
    }

}