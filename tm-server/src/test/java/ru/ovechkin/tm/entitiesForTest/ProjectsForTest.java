package ru.ovechkin.tm.entitiesForTest;

import ru.ovechkin.tm.constant.TestConst;
import ru.ovechkin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectsForTest {

    private Project createProject(
            final String userId,
            final String id,
            final String name,
            final String description
    ) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    final Project project1 = createProject(
            TestConst.USER_ID_USER, "testId1", "projectName1", "projectDescription");

    final Project project2 = createProject(
            TestConst.USER_ID_USER, "testId2", "projectName2", "projectDescription2");

    final Project project3 = createProject(
            TestConst.USER_ID_ADMIN, "testId11", "projectForAdmin1", "projectDescription1");

    public List<Project> getListOfTestProjects() {
        final List<Project> testProjectList = new ArrayList<>();
        testProjectList.add(project1);
        testProjectList.add(project2);
        testProjectList.add(project3);
        return testProjectList;
    }

    public Project getProject1() {
        return project1;
    }

    public Project getProject2() {
        return project2;
    }

    public Project getProject3() {
        return project3;
    }

}