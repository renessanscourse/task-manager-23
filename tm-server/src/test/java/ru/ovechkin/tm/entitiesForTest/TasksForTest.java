package ru.ovechkin.tm.entitiesForTest;

import ru.ovechkin.tm.constant.TestConst;
import ru.ovechkin.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TasksForTest {

    private Task createTask(
            final String userId,
            final String id,
            final String name,
            final String description
    ) {
        final Task task = new Task();
        task.setUserId(userId);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    final Task task1 = createTask(
            TestConst.USER_ID_USER, "testId1", "taskName1", "taskDescription");

    final Task task2 = createTask(
            TestConst.USER_ID_USER, "testId2", "taskName2", "taskDescription2");

    final Task task3 = createTask(
            TestConst.USER_ID_ADMIN, "testId11", "taskForAdmin1", "taskDescription1");

    public List<Task> getListOfTestTasks() {
        final List<Task> taskList = new ArrayList<>();
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task3);
        return taskList;
    }

    public Task getTask1() {
        return task1;
    }

    public Task getTask2() {
        return task2;
    }

    public Task getTask3() {
        return task3;
    }

}