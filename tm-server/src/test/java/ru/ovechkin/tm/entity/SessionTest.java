package ru.ovechkin.tm.entity;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.constant.TestConst;

public class SessionTest {

    private final Session testSession = new Session();

    @Test
    public void testId() {
        Assert.assertNotNull(testSession.getId());
        testSession.setId(TestConst.TEST_STRING);
        Assert.assertNotNull(testSession.getId());
        Assert.assertEquals(TestConst.TEST_STRING, testSession.getId());
    }

    @Test
    public void testTimeStamp() {
        Assert.assertNull(testSession.getTimestamp());
        final Long testLong = System.currentTimeMillis();
        testSession.setTimestamp(testLong);
        Assert.assertNotNull(testSession.getTimestamp());
        Assert.assertEquals(testLong, testSession.getTimestamp());
    }

    @Test
    public void testUserId() {
        Assert.assertNull(testSession.getUserId());
        testSession.setUserId(TestConst.TEST_STRING);
        Assert.assertNotNull(testSession.getUserId());
        Assert.assertEquals(TestConst.TEST_STRING, testSession.getUserId());
    }

    @Test
    public void testSignature() {
        Assert.assertNull(testSession.getSignature());
        testSession.setSignature(TestConst.TEST_STRING);
        Assert.assertNotNull(testSession.getSignature());
        Assert.assertEquals(TestConst.TEST_STRING, testSession.getSignature());
    }

}