package ru.ovechkin.tm.entity;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import static ru.ovechkin.tm.constant.TestConst.TEST_STRING;

public class TaskTest {

    private final Task task = new Task();

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Test(timeout = 5)
    public void testId() {
        try {
            Assert.assertNotNull(task.getId());
            task.setId(TEST_STRING);
            Assert.assertEquals(TEST_STRING, task.getId());
        } catch (Exception e) {
            collector.addError(e);
        }
    }

    @Test
    public void testProjectUserId() {
        task.setUserId(TEST_STRING);
        Assert.assertNotNull(task.getUserId());
        Assert.assertEquals(TEST_STRING, task.getUserId());
    }

    @Test
    public void testProjectName() {
        Assert.assertNotNull(task.getName());
        task.setName(TEST_STRING);
        Assert.assertEquals(TEST_STRING, task.getName());
    }

    @Test
    public void testProjectDescription() {
        Assert.assertNotNull(task.getDescription());
        task.setDescription(TEST_STRING);
        Assert.assertEquals(TEST_STRING, task.getDescription());
    }

}