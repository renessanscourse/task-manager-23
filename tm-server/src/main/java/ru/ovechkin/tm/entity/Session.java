package ru.ovechkin.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session extends AbstractEntity implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getStackTrace());
            return null;
        }
    }

    @NotNull
    private Long timestamp;

    @NotNull
    private String userId;

    @NotNull
    private String signature;

    public Session() {
    }

    @Override
    public String toString() {
        return "Session{" +
                "timestamp=" + timestamp +
                ", userId='" + userId + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }

    @NotNull
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NotNull Long timestamp) {
        this.timestamp = timestamp;
    }

    @NotNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

    @NotNull
    public String getSignature() {
        return signature;
    }

    public void setSignature(@NotNull String signature) {
        this.signature = signature;
    }

}