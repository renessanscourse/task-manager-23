package ru.ovechkin.tm.entity;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public final class Task extends AbstractEntity implements Serializable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}