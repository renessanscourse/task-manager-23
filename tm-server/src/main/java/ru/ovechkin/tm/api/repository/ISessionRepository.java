package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable Session findById(@NotNull String id);

    @Nullable List<Session> findByUserId(@NotNull String userId);

    boolean contains(@NotNull String id);

    void remove(@NotNull Session session);

    void removeByUserId(@NotNull String userId);

}