package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void add(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "project", partName = "project") Project project);

    @WebMethod
    void createProjectWithName(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void createProjectWithNameAndDescription(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @Nullable
    @WebMethod
    List<Project> findUserProjects(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void removeAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @Nullable
    @WebMethod
    Project findProjectById(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    Project findProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    Project findProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @NotNull
    @WebMethod
    Project updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @NotNull
    @WebMethod
    Project updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @NotNull
    @WebMethod
    Project removeProjectById(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @NotNull
    @WebMethod
    Project removeProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @NotNull
    @WebMethod
    Project removeProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

}