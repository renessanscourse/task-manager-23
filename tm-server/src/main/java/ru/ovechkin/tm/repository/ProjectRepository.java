package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public final void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        entities.add(project);
    }

    @NotNull
    @Override
    public final List<Project> findUserProjects(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (final Project project: entities) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public final void removeAll(@NotNull final String userId) {
        @NotNull final List<Project> projects = findUserProjects(userId);
        this.entities.removeAll(projects);
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Project project: entities) {
            if (userId.equals(project.getUserId())) {
                if (id.equals(project.getId())) return project;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Project findByIndex(@NotNull final String userId,@NotNull final Integer index) {
        final @Nullable List<Project> userProjects = findUserProjects(userId);
        for (@NotNull final Project project: userProjects) {
            if (userId.equals(project.getUserId())) {
                if ((userProjects.indexOf(project) + 1) == index) return project;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Project findByName(@NotNull final String userId,@NotNull final String name) {
        for (@NotNull final Project project: entities) {
            if (userId.equals(project.getUserId())) {
                if (name.equals(project.getName())) return project;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId,@NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        entities.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByIndex(@NotNull final String userId,@NotNull final Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        entities.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull final String userId,@NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        entities.remove(project);
        return project;
    }

}