package ru.ovechkin.tm;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.bootstrap.Bootstrap;

import java.lang.Exception;

public class Client {

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}