package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_CREATE;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().
                createProjectWithNameAndDescription(session, name, description);
        System.out.println("[OK]");
    }

}