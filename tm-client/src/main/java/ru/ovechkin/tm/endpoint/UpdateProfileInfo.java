
package ru.ovechkin.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateProfileInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateProfileInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.tm.ovechkin.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="newLogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="newFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="newMiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="newLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="newEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateProfileInfo", propOrder = {
    "session",
    "newLogin",
    "newFirstName",
    "newMiddleName",
    "newLastName",
    "newEmail"
})
public class UpdateProfileInfo {

    protected Session session;
    protected String newLogin;
    protected String newFirstName;
    protected String newMiddleName;
    protected String newLastName;
    protected String newEmail;

    /**
     * Gets the value of the session property.
     * 
     * @return
     *     possible object is
     *     {@link Session }
     *     
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     * 
     * @param value
     *     allowed object is
     *     {@link Session }
     *     
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the newLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewLogin() {
        return newLogin;
    }

    /**
     * Sets the value of the newLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewLogin(String value) {
        this.newLogin = value;
    }

    /**
     * Gets the value of the newFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewFirstName() {
        return newFirstName;
    }

    /**
     * Sets the value of the newFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewFirstName(String value) {
        this.newFirstName = value;
    }

    /**
     * Gets the value of the newMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewMiddleName() {
        return newMiddleName;
    }

    /**
     * Sets the value of the newMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewMiddleName(String value) {
        this.newMiddleName = value;
    }

    /**
     * Gets the value of the newLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewLastName() {
        return newLastName;
    }

    /**
     * Sets the value of the newLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewLastName(String value) {
        this.newLastName = value;
    }

    /**
     * Gets the value of the newEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewEmail() {
        return newEmail;
    }

    /**
     * Sets the value of the newEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewEmail(String value) {
        this.newEmail = value;
    }

}
