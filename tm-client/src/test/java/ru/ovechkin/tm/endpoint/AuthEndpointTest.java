package ru.ovechkin.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.locator.EndpointLocator;

public class AuthEndpointTest {

    private final IEndpointLocator endpointLocator = new EndpointLocator();

    private final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();

    @Test
    public void testLogin() {
        final Session sessionOfUser = authEndpoint.login("user", "user");
        Assert.assertTrue(authEndpoint.isAuth(sessionOfUser));
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testRegistry() {
        authEndpoint.registry("test", "test", "test");
        final Session sessionOfUser = authEndpoint.login("test", "test");
        Assert.assertTrue(authEndpoint.isAuth(sessionOfUser));
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testShowCurrentUser() {
        final Session sessionOfAdmin = authEndpoint.login("admin", "admin");
        final User user = endpointLocator.getUserEndpoint().findByLogin(sessionOfAdmin, "admin");
        Assert.assertEquals(user.toString(), authEndpoint.showCurrentUserByUserId(sessionOfAdmin).toString());
        authEndpoint.logout(sessionOfAdmin);
    }

    @Test
    public void testUpdatePassword() {
        final Session sessionOfUser = authEndpoint.login("user", "user");
        authEndpoint.updatePassword(sessionOfUser, "user", "user1");
        authEndpoint.logout(sessionOfUser);
        authEndpoint.login("user", "user1");
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testUpdateProfileInfo() {
        final Session sessionOfUser = authEndpoint.login("user", "user1");
        authEndpoint.updateProfileInfo(
                sessionOfUser,
                "newLogin",
                "newFirstname",
                "newMiddleName",
                "newLastName",
                "newEmail");
        Assert.assertEquals("newLogin", authEndpoint.showCurrentUserByUserId(sessionOfUser).getLogin());
        Assert.assertEquals("newFirstname", authEndpoint.showCurrentUserByUserId(sessionOfUser).getFirstName());
        Assert.assertEquals("newMiddleName", authEndpoint.showCurrentUserByUserId(sessionOfUser).getMiddleName());
        Assert.assertEquals("newLastName", authEndpoint.showCurrentUserByUserId(sessionOfUser).getLastName());
        Assert.assertEquals("newEmail", authEndpoint.showCurrentUserByUserId(sessionOfUser).getEmail());
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testIsAuth() {
        final Session sessionOfUser = authEndpoint.login("newLogin", "user1");
        Assert.assertTrue(authEndpoint.isAuth(sessionOfUser));
        authEndpoint.logout(sessionOfUser);
    }

    @Test
    public void testLogout() {
        final Session sessionOfUser = authEndpoint.login("newLogin", "user1");
        Assert.assertTrue(authEndpoint.isAuth(sessionOfUser));
        authEndpoint.logout(sessionOfUser);
        Assert.assertFalse(authEndpoint.isAuth(sessionOfUser));
    }

    @Test
    public void testGetUserId() {
        final Session sessionOfUser = authEndpoint.login("newLogin", "user1");
        Assert.assertEquals(sessionOfUser.getUserId(), authEndpoint.getUserId(sessionOfUser));
        authEndpoint.logout(sessionOfUser);
    }

}